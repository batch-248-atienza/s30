//A

db.fruits.aggregate([
	{$match: 
		{$and: [
				{onSale: true},
				{stock: {$gte: 20}}
			]}
	},
	{$count: "enoughStock"}
]);

//B

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: { _id:"$supplier_id", avg_price:{$avg: "$price"} } },
	{$sort: {avg_price: -1}}
]);

//C

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: { _id:"$supplier_id", max_price:{$max: "$price"} } },
	{$sort: {max_price: 1}}
]);

//D

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: { _id:"$supplier_id", min_price:{$min: "$price"} } },
	{$sort: {min_price: -1}}
]);